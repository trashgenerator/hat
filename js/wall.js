Hat.Wall = atom.Class({
            Implements: [ Drawable ],
            Extends: Hat.Sprite,
            image_name:'wall',
            initialize: function(controller, position) {
                this.parent(controller);
                if (position) {
                    this.initial_position = position;
                } 
                return this;
            },
            libcanvasSet_callback: function() {
                if (!this.initial_position) {
                    this.initial_position = new Point(
                            Number.random(0, this.controller.field.width),
                            Number.random(0, this.controller.field.height)
                    );

                }
                this.parent();
            },
            sprite_groups: function() {
                return this.parent().concat(Array('walls'));
            }
        });