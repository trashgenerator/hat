Hat.Enemy = atom.Class({
            Implements: [ Drawable ],
            Extends: Hat.Sprite,
            image_name:'hero',
            animation_name:'zombee_animation',
            health_max:100,
            image_died_name:'died_zombee',
            initialize: function(controller, position) {
                this.parent(controller);
                if (position) {
                    this.initial_position = position;
                }
                this.speed = 150;
                this.angle = (-45).degree();
                return this;
            },
            libcanvasSet_callback: function() {
                if (!this.initial_position) {
                    this.initial_position = new Point(
                            Number.random(0, this.controller.field.width),
                            Number.random(0, this.controller.field.height)
                    );

                }
                this.parent();
            },
            sprite_groups: function() {
                return this.parent().concat(Array('enemies'));
            },
            update: function (time) {
                if(this.shape.intersect(this.controller.hero.shape)){
                        var audio=this.libcanvas.getAudio('hero_killed');
                        audio.playNext();
                    this.controller.finish(false);
                }
                if (this.controller.bullets) {
                    $.each(this.controller.bullets, function(i, bullet) {
                        if (bullet) {
                            if (bullet.shape.intersect(this.shape)) {
                                bullet.die();
                                this.attacked(bullet);
                                return false;
                            }
                        }
                    }.bind(this));

                }
                if (this.died) {
                    return;
                }
                time = time.toSeconds();
                var new_angle, new_velocity, new_moveToShape;
                var is_path_finded = false;
                var velocity = this.get_velocity().mul(time);
                var moveToShape = this.shape.clone().move(velocity);
                if (this.controller.field.is_collide_uncolideble(moveToShape)) {
                    for (var i = 0; i < 10; i++) {
                        new_angle = Number.random(0, 359).degree();
                        velocity = this.get_velocity(new_angle).mul(time);
                        new_moveToShape = this.shape.clone().move(velocity);
                        if (!this.controller.field.is_collide_uncolideble(new_moveToShape)) {
                            this.angle = new_angle;
                            is_path_finded = true;
                            break;
                        }

                    }
                } else {
                    is_path_finded = true;
                }
                if (!is_path_finded) {
                    this.angle -= Math.PI;
                    velocity = this.get_velocity(new_angle).mul(time);
                }
                this.shape.move(velocity);
            },
            attacked: function(attacker) {
                this.health -= attacker.power;
                var blood_angle=attacker.angle+Number.random(0,30).degree()*(new Array(-1,1)).random;
                new Hat.Blood(this.controller, this.shape.getCenter(), blood_angle);
                if (this.health<=0) {
                    this.die();
                }
            },
            die: function() {

                        var audio=this.libcanvas.getAudio('killed');
                        audio.playNext();
                this.parent();
            }



        });