Hat.Blood = atom.Class({
            Implements: [ Drawable ],
            Extends: Hat.Sprite,
            image_name:'blood',
            image_died_name:'blood',
            name: 'blood',
            initialize: function(controller, initial_position, angle) {
                this.parent(controller);
                if (initial_position) {
                    this.initial_position = initial_position;
                }
                this.speed = 200;
                this.angle = angle;
                this.speed_sizeincrease=300;
                this.life_time = 0.2;
                return this;
            },
            libcanvasSet_callback: function() {
                this.parent()
                this.shape.setWidth(0);
                this.shape.setHeight(0);

            },
            sprite_groups: function() {
                return this.parent().concat(Array('bloods'));
            },
            update: function (time) {
                time = time.toSeconds();
                this.life_time -= time;
                if(this.life_time<=0){
                    this.die();
                    return;
                }
                this.shape.setWidth(this.shape.getWidth()+this.speed_sizeincrease * time);
                this.shape.setHeight(this.shape.getHeight()+this.speed_sizeincrease * time);
                var velocity = this.get_velocity().mul(time);
                var moveToShape = this.shape.clone().move(velocity);
                this.shape.move(velocity);
            }
        });