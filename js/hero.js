Hat.Hero = atom.Class({
            Implements: [ Drawable ],
            Extends: Hat.Sprite,
            image_name: 'niga',
            freeze_time: 0.1,
            speed:300,
            libcanvasSet_callback: function() {
                this.angle_vision = 0;
                this.reload = 0;
                this.initial_position = new Point(this.libcanvas.ctx.width / 2, this.libcanvas.ctx.height / 2);
                this.libcanvas.mouse.addEvent('move', this.mousemove.bind(this));
                this.parent();
            },
            update: function (time) {
//		this.move(time);


                var key = this.libcanvas.getKey.context(this.libcanvas);

                time = time.toSeconds();
                var speed_vector, distanse_x = 0, distanse_y = 0, moveToShape,moving = false;

                // Move
//                if ((key('w') || key('s')) && (key('a') || key('d'))) {
//                    distanse_x = distanse_y = Math.sqrt(this.speed * this.speed / 2);
//                    if (key('a')) distanse_x *= -1;
//                    if (key('w')) distanse_y *= -1;
//                } else if (key('w') || key('s')) {
//                    distanse_y = this.speed;
//                    if (key('w')) distanse_y *= -1;
//                } else if (key('a') || key('d')) {
//                    distanse_x = this.speed;
//                    if (key('a')) distanse_x *= -1;
//                }
//                if (distanse_x != 0 || distanse_y != 0) {
//                    speed_vector = new Point(distanse_x, distanse_y).mul(time);
//                    moveToShape = this.shape.clone().move(speed_vector);
//
//                    speed_vector = this.checkBounds(moveToShape, speed_vector.clone());
//                    speed_vector = this.checkWalls(moveToShape, speed_vector.clone());
//                    this.shape.move(speed_vector);
//                    this.controller.field.set_move(speed_vector);
//                }
                if (key('d') || key('w') || key('a') || key('s')) {
                    if (key('d')) this.angle = 0;
                    if (key('a')) this.angle = 180;
                    if (key('w')) this.angle = 270;
                    if (key('s')) this.angle = 90;
                    if (key('d') && key('w')) this.angle = 315;
                    if (key('w') && key('a')) this.angle = 225;
                    if (key('a') && key('s')) this.angle = 135;
                    if (key('s') && key('d')) this.angle = 45;
                    this.angle = this.angle.degree();
                    var velocity = this.get_velocity().mul(time);
                    moveToShape = this.shape.clone().move(velocity);
                    velocity = this.checkBounds(moveToShape, velocity.clone());
                    velocity = this.checkWalls(moveToShape, velocity.clone());
                    this.shape.move(velocity);
                    this.controller.field.set_move(velocity);
                }
                this.reload = (this.reload - time).limit(0);
                if (this.libcanvas.mouse.button('left')) {
                    if (this.reload == 0) {
                        console.log('fire');
                        var audio=this.libcanvas.getAudio('shot');
                        audio.playNext();
                        new Hat.Bullet(this.controller, this.get_gun_position(), this.angle_vision);
                        this.reload = this.freeze_time;
                    }
                }

//		this.checkCollisions();
            },

            mousemove: function(e) {
                this.angle_vision = this.libcanvas.mouse.point.angleTo(this.shape.getCenter());

            },
            draw: function () {
                this.libcanvas.ctx.stroke(this.shape, 'black');
                this.libcanvas.ctx.drawImage({
                            'image':this.image,
                            'draw':this.shape,
                            'angle':this.angle_vision});
            },
            get_gun_position: function() {
                return this.shape.getCenter().clone().move(this.get_velocity(this.angle_vision+(40).degree(), 25))

            },
            /**
             * Проверяет, не вылезли ли не пытается ли объект вылезти за границу экрана
             * и корректирует вектор скорости
             * @param speed_vector Вектор движения
             */
            checkBounds: function (moveToShape, speed_vector) {
                if (moveToShape.to.y > this.controller.field.shape.to.y ||
                        moveToShape.from.y < this.controller.field.shape.from.y) {
                    speed_vector.y = 0
                }
                if (moveToShape.to.x > this.controller.field.shape.to.x ||
                        moveToShape.from.x < this.controller.field.shape.from.x) {
                    speed_vector.x = 0
                }
                return speed_vector;
            }
            ,
            /**
             * Если тыкаемся в стенку под наклоном, то едем в возможную сторону
             * Если тыкаемся по прямой, то останавливаемся
             * @param moveToShape
             * @param speed_vector
             */
            checkWalls: function(moveToShape, speed_vector) {
                var speed_vector_tmp = speed_vector.clone();
                var intersects = false;
                $.each(this.controller.walls, function(i, wall) {
                    if (moveToShape.intersect(wall.shape)) {
                        intersects = false;
//                        atom.log('a')
                        // если едем в бок, то просто останавливаемся
                        if ((speed_vector.x != 0 && speed_vector.y == 0) || (speed_vector.y != 0 && speed_vector.x == 0)) {
                            speed_vector = new Point(0, 0);
                            return false;
                        }
                        // отрубаем скорость по оси х
                        speed_vector_tmp.x = 0;
                        moveToShape = this.shape.clone().move(speed_vector_tmp);

                        $.each(this.controller.walls, function(i, wall) {
                            if (moveToShape.intersect(wall.shape)) {
                                intersects = true;
                                return false;
                            }
                        }.bind(this));
                        // если не занято, туда и дорога
                        if (!intersects) {
                            speed_vector = speed_vector_tmp.clone();
                            return false;
                        } else {
                            intersects = false;
                            // если там занято, пытаемся двигаться по y
                            speed_vector_tmp = speed_vector.clone();
                            speed_vector_tmp.y = 0;
                            moveToShape = this.shape.clone().move(speed_vector_tmp);

                            $.each(this.controller.walls, function(i, wall) {
                                if (moveToShape.intersect(wall.shape)) {
                                    intersects = true;
                                    return false;
                                }
                            }.bind(this));
                            // если и там занято стопим
                            if (intersects) {
                                speed_vector = new Point(0, 0);
                                return false;
                            } else {
                                speed_vector = speed_vector_tmp.clone();
                                return false;
                            }
                        }
                    }

                }.bind(this));
                return speed_vector;
            }

        });