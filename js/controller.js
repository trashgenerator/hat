Hat.Controller = atom.Class({

            initialize: function (canvas) {

                this.libcanvas = new LibCanvas(canvas, {
                            fps: 60,
                            clear: false,
                            backBuffer: 'off',
                            preloadImages: { hero : 'images/hat.png',
                                field: 'images/field.png',
                                wall:'images/wall.png',
                                bullet:'images/bullet.png',
                                blood:'images/blood.png',
                                enemy_dead:'images/enemy_dead.png',
                                zombee_animation:'images/zombee-animation.png',
                                niga:'images/niga.png',
                                bullet_boom:'images/bullet_boom.png',
                                died_zombee:'images/died-zombee.png'
                            },
                            preloadAudio: {
                                shot      : 'sounds/tish1.*',
                                killed      : 'sounds/sdohni.*',
                                hero_killed      : 'sounds/aaa.*',
                                win      : 'sounds/eee.*'
                            }
                        })
                        .listenKeyboard([ 'aup', 'adown', 'w', 's' ])
                        .listenMouse()
                        .addEvent('ready', this.start.bind(this))
                        .start()
                        .fpsMeter();
            },

            start: function () {

			this.libcanvas.getAudio('shot').gatling(1);
			this.libcanvas.getAudio('killed').gatling(1);
			this.libcanvas.getAudio('hero_killed').gatling(1);
			this.libcanvas.getAudio('win').gatling(1);
                var libcanvas = this.libcanvas;
                this.field = new Hat.Field(this);
                this.hero = new Hat.Hero(this);


                libcanvas
                        .size({
                            width : 800,
                            height: 500
                        }, true)
                        .addFunc(function (time) {
                    if (this.enemies && this.enemies.length == 0) {
                        var audio=this.libcanvas.getAudio('win');
                        audio.playNext();

                        this.finish(true);
                    }
//                    this.hero.update(time);
                    this.sprites.invoke('update', time);
                    this.field.moveAll();
                    libcanvas.update();
                }.context(this));

                for (var i = 0; i < 50; i++) {
                    var wall = new Hat.Wall(this);

                }
                for (i = 0; i < 20; i++) {
                    var enemy = new Hat.Enemy(this);

                }
                var libcanvas_background = libcanvas.createLayer('libcanvas_background');
                libcanvas_background.zIndex = 0; // опускаем слой в самый низ

//                libcanvas_background.zIndex = Infinity; // поднимаем слой в самый верх
                libcanvas_background.addElement(this.field)
                        .size({
                            width : 800,
                            height: 500
                        })
                        .addFunc(function (time) {
                    libcanvas_background.update();
                }.context(this));
//                libcanvas.libcanvas_backgroundLayer == libcanvas_background; // true
            },
            finish: function(win) {
                var msg;
                if (win) {
                    msg = 'Победил чо молодец тыкай F5 и убей их еще раз';
                }
                else {
                    msg = 'тебя съели неудачник тыкай F5 и отомсти тварям'
                }
                $('#msg').html('<h1>' + msg + '</h1>')
                this.libcanvas.stop()
            }
        });
