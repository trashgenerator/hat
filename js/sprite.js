Hat.Sprite = atom.Class({
            Implements: [ Drawable ],
            image_name: undefined,
            animation_name: undefined,
            initial_position: undefined,
            health_max:1,
            image_died_name:undefined,
            initialize: function(controller) {
                this.health = this.health_max;
                this.angle = 0;
                this.died = false;
                this.controller = controller;
                this.addEvent('libcanvasSet', this.libcanvasSet_callback);
                this.controller.libcanvas
                        .addElement(this);
                return this;
            },
            libcanvasSet_callback: function() {
                $.each(this.sprite_groups(), function(i, sprite_group) {
                    if (!this.controller[sprite_group]) {
                        this.controller[sprite_group] = Array();
                    }
                    this.controller[sprite_group].push(this)
                }.bind(this));
                if (this.animation_name) {

                    this.animation = new Animation()
                            .addSprites(this.libcanvas.getImage(this.animation_name), 50)
                            .run({
                                line : Array.range(0, 3),
                                delay: 80,
                                loop : true
                            });
                    this.shape = new Rectangle(
                            this.initial_position.x, this.initial_position.y,
                            50, 50
                    );


                } else {
                    var img = this.libcanvas.getImage(this.image_name);
                    this.image = img.sprite(0, 0, img.width, img.height);
                    this.shape = new Rectangle(
                            this.initial_position.x, this.initial_position.y, img.width, img.height
                    );
                }


            },
            draw: function () {
                if (this.animation_name) {
                    image=this.animation.getSprite();
                } else {
                    var image = this.image;
                }
                this.libcanvas.ctx.stroke(this.shape, 'black');
                this.libcanvas.ctx.drawImage({
                            'image':image,
                            'draw':this.shape,
                            'angle':this.angle});
            },
            sprite_groups: function() {
                return Array('sprites');
            },
            get_velocity : function (angle, speed) {
                if (!angle) angle = this.angle;
                if (!speed) speed = this.speed;
                return new Point(
                        angle.cos() * speed,
                        angle.sin() * speed
                );
            },
            update: function(time) {

            },
            die: function() {
                this.died = true;
                if (this.image_died_name) {
                    this.controller.field.draw_on_self(this.image_died_name, this.shape, this.angle)
                }
                this.libcanvas.rmElement(this);
                $.each(this.sprite_groups(), function(i, group) {
                    this.controller[group].erase(this);
                }.bind(this));
            }

        });