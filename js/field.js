Hat.Field = atom.Class({
            Implements: [ Drawable ],
            width:1024,
            height:2000,

            initialize: function(controller) {
                this.controller = controller;
                this.addEvent('libcanvasSet', function () {
                    this.image = this.libcanvas.getImage('field').sprite();
                    this.shape = new Rectangle(
                            0, 0, 1024, 2000
                    );

                });
                return this;
            },
            draw: function () {
                this.libcanvas.ctx.drawImage(this.image, this.shape);
            },
            set_move: function(speed_vector) {
                this.speed_vector = speed_vector.clone();
            },
            is_out: function(shape) {
                return (shape.from.x < this.shape.from.x || shape.from.y < this.shape.from.y || shape.to.x > this.shape.to.x || shape.to.y > this.shape.to.y )
            },
            is_collide_walls: function(shape) {
                var intersects = false;
                $.each(this.controller.walls, function(i, wall) {
                    if (shape.intersect(wall.shape)) {
                        intersects = true;
                        return false;
                    }
                });
                return intersects;
            },
            is_collide_uncolideble: function(shape) {
                return this.is_out(shape) || this.is_collide_walls(shape);
            },
            moveAll: function() {
                if (this.speed_vector) {
                    var speed_vector_opposite = this.speed_vector.clone();
                    speed_vector_opposite.x *= -1;
                    speed_vector_opposite.y *= -1;
                    var moveToShape = this.shape.clone().move(speed_vector_opposite);
//                    atom.log(moveToShape.from.x);
                    //останавливаем у стенок
                    if (moveToShape.from.x >= 0) {
                        speed_vector_opposite.x = 0;
                    }
                    if (moveToShape.to.x <= this.libcanvas.ctx.width) {
                        speed_vector_opposite.x = 0;
                    }
                    if (moveToShape.from.y > 0 || moveToShape.to.y < this.libcanvas.ctx.height) {
                        speed_vector_opposite.y = 0;
                    }

                    //даём дойти до середины от стенок
                    if (this.controller.hero.shape.from.x < this.libcanvas.ctx.width / 2 && speed_vector_opposite.x < 0) {
                        speed_vector_opposite.x = 0;

                    } else if (this.controller.hero.shape.from.x > this.libcanvas.ctx.width / 2 && speed_vector_opposite.x > 0) {
                        speed_vector_opposite.x = 0;

                    }

                    if (this.controller.hero.shape.from.y < this.libcanvas.ctx.height / 2 && speed_vector_opposite.y < 0) {
                        speed_vector_opposite.y = 0;

                    } else if (this.controller.hero.shape.from.y > this.libcanvas.ctx.height / 2 && speed_vector_opposite.y > 0) {
                        speed_vector_opposite.y = 0;

                    }
                    if (speed_vector_opposite.x != 0 || speed_vector_opposite.y != 0) {
                        this.shape.move(speed_vector_opposite.clone());
                        this.controller.sprites.forEach(function(sprite) {
                            sprite.shape.move(speed_vector_opposite.clone())
                        }.bind(this));
                    }
                }
                this.speed_vector = null;
            },
            draw_on_self: function(image_died_name, shape, angle) {
                var img = this.libcanvas.getImage(image_died_name);
                shape.move(new Point(
                        ((-1) * this.shape.from.x),
                        ((-1) * this.shape.from.y)));
                this.image.ctx.drawImage({
                            'image':img,
                            'draw':shape,
                            'angle':angle});

            }

        });