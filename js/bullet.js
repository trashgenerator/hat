Hat.Bullet = atom.Class({
            Implements: [ Drawable ],
            Extends: Hat.Sprite,
            image_name:'bullet',
            image_explode_name:'bullet_boom',
            power:20,
            initialize: function(controller, initial_position, angle) {
                this.parent(controller);
                if (initial_position) {
                    this.initial_position = initial_position;
                }
                this.speed = 500;
                this.name ='bullet';
                this.angle = angle;
                return this;
            },
            sprite_groups: function() {
                return this.parent().concat(Array('bullets'));
            },
            update: function (time) {
                time = time.toSeconds();
                var new_angle, new_velocity, new_moveToShape;
                var is_path_finded=false;
                var velocity = this.get_velocity().mul(time);
                var moveToShape = this.shape.clone().move(velocity);
                if (this.controller.field.is_collide_uncolideble(moveToShape)) {
                    this.die();
                    return
                }
                this.shape.move(velocity);
            }
//            ,
//    die:function(){
//
//    }
        });